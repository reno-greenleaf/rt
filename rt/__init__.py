from __future__ import absolute_import, unicode_literals
from .celery_app import app as c_app

__all__ = ('c_app',)