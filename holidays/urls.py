from django.conf.urls import url
from views import Holidays

urlpatterns = [
  url(r'holidays/(?P<month>\d{4}-\d{2})$', Holidays.as_view(), name='holidays')
]
