# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from calendar_user.models import Country

class Holiday(models.Model):
  """ Special event in calendar. """
  uid = models.PositiveIntegerField(primary_key=True)
  name = models.CharField(max_length=512)
  start = models.DateTimeField()
  end = models.DateTimeField(null=True)
  duration = models.DurationField(null=True)
  description = models.TextField(null=True)
  country = models.ForeignKey(Country, related_name='holidays')
  url = models.URLField(null=True)
