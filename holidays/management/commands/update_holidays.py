from django.core.management.base import BaseCommand
from holidays.tasks import update_holidays as update

class Command(BaseCommand):
  help = "Get/update holidays."

  def handle(self, *args, **options):
    update()