from __future__ import absolute_import, unicode_literals

from ics import Calendar
from requests import get
from celery import shared_task
from calendar_user.models import Country
from holidays.models import Holiday

@shared_task
def update_holidays():
  """ Get all holidays. """
  countries = Country.objects.all()

  for country in countries:
    print country.name,
    response = get('https://www.officeholidays.com/ics/ics_country.php', params={'tbl_country': country.name})

    if response.status_code != 200:
      print "- failed to access data"
      continue

    try:
      calendar = Calendar(response.text)
    except Exception:
      print '- failed to get events'
      continue

    print "- retrieved %d events" % len(calendar.events)

    for event in calendar.events:
      name = event.name.replace(country.name+': ', '')
      defaults = {
        'name': name,
        'start': event.begin.datetime,
        'end': event.end.datetime,
        'duration': event.duration,
        'description': event.description,
        'country': country,
        'url': event.url
      }
      Holiday.objects.update_or_create(uid=event.uid, defaults=defaults)
