# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db.models import Q
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from serializers import HolidaySerializer

class Holidays(APIView):
  """ List holidays by month. """
  permission_classes = (IsAuthenticated,)
  authentication_classes = (TokenAuthentication,)

  def get(self, request, month):
    country = request.user.country
    year, month_ = month.split('-')
    start = Q(start__year=year) & Q(start__month=month_)
    end = Q(end__year=year) & Q(end__month=month_)
    holidays = country.holidays.filter(start | end).all()
    serializer = HolidaySerializer(holidays, many=True)
    return Response(serializer.data)
