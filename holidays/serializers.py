from rest_framework.serializers import ModelSerializer, CharField
from models import Holiday

class HolidaySerializer(ModelSerializer):
  """ Serializes holiday data. """
  country_name = CharField(read_only=True, source='country.name')

  class Meta:
    model = Holiday
    fields = ('uid', 'name', 'start', 'end', 'duration', 'description', 'country_name', 'url')