# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.dateparse import parse_datetime as p
from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework.authtoken.models import Token
from calendar_user.models import Country, User
from models import Holiday

class TestAPI(TestCase):
  def setUp(self):
    self.client = APIClient()
    self.country = Country.objects.create(name='Belarus')
    user = User.objects.create_user('Max', password='max123123', country=self.country)
    token = Token.objects.create(user=user)
    self.client.credentials(HTTP_AUTHORIZATION='Token '+token.key)

  def test_get_holidays(self):
    Holiday.objects.create(uid=1,
                           name='This month',
                           start=p('2019-02-15 00:00+03:00'),
                           end=p('2019-02-16 00:00+03:00'),
                           country=self.country)
    Holiday.objects.create(uid=2,
                           name='Other month',
                           start=p('2019-01-15 00:00+03:00'),
                           end=p('2019-01-16 00:00+03:00'),
                           country=self.country)
    response = self.client.get('/events/holidays/2019-02')
    names = [holiday['name'] for holiday in response.data]
    self.assertIn('This month', names)
    self.assertNotIn('Other month', names)
