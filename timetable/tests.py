# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import timedelta
from django.test import TestCase
from calendar_user.models import User, Country
from django.utils.dateparse import parse_datetime as p
from rest_framework.test import APIClient
from rest_framework.authtoken.models import Token
from models import Event

class TestModel(TestCase):
  def setUp(self):
    country = Country.objects.create(name='Belarus')
    self.user = User.objects.create_user('Bob', email='bob@email.com', password='bob123123', country=country)

  def test_create_event(self):
    event = Event(name='Do this',
                  start=p('2019-02-11 12:00Z'),
                  end=p('2019-02-11 13:00Z'),
                  remind=timedelta(seconds=3600),
                  author=self.user)
    event.save()
    self.assertIsNotNone(event.pk)

  def test_without_end(self):
    event = Event(name='Do that',
              start=p('2019-02-11 12:00Z'),
              author=self.user)
    event.save()
    event = Event.objects.get(name='Do that')
    self.assertEqual(event.end, p('2019-02-11 23:59:59Z'))


class TestAPI(TestCase):
  def setUp(self):
    self.client = APIClient()
    country = Country.objects.create(name='Neverland')
    self.user = User.objects.create_user('John', email='john@email.com', password='john123123', country=country)
    token = Token.objects.create(user=self.user)
    self.client.credentials(HTTP_AUTHORIZATION='Token '+token.key)

  def test_create_event(self):
    name = 'Test creating event'
    self.client.post('/events/', {'name': name, 'start': '2019-02-11 12:00Z', 'end': '2019-02-11 14:00Z', 'remind': timedelta(seconds=7200)})
    created = Event.objects.filter(name=name).all()
    self.assertEqual(len(created), 1)

  def test_get_event(self):
    event = Event(name='Test getting event', start=p('2019-02-11 12:00Z'), author=self.user)
    event.save()
    response = self.client.get('/events/%d/' % event.pk)
    self.assertEqual(response.data['name'], 'Test getting event')

  def test_update_event(self):
    event = Event(name='Test updating event', start=p('2019-01-11 12:00Z'), author=self.user)
    event.save()
    new_end = '2019-01-11 16:00Z'
    self.client.patch('/events/%d/' % event.pk, {'end': new_end})
    event.refresh_from_db()
    self.assertEqual(event.end, p(new_end))

  def test_delete_event(self):
    event = Event(name='Test deleting event', start=p('2019-01-12 12:45Z'), author=self.user)
    event.save()
    self.client.delete('/events/%d/' % event.pk)
    events = Event.objects.filter(pk=event.pk)
    self.assertEqual(len(events), 0)

  def test_event_with_invalid_reminder(self):
    data = {'name': 'Test wrong reminder', 'start': '2019-01-11 12:00Z', 'end': '2019-01-12 12:00Z', 'remind': timedelta(days=3)}
    response = self.client.post('/events/', data)
    self.assertEqual(response.status_code, 400)

  def test_create_event_with_wrong_dates(self):
    response = self.client.post('/events/', {'name': 'Test wrong dates', 'start': '2019-01-11 12:00Z', 'end': '2019-01-10 12:00Z'})
    self.assertEqual(response.status_code, 400)

  def test_update_event_with_wrong_dates(self):
    event = Event(name='Test updating event with wrong dates', start=p('2019-01-11 12:00Z'), author=self.user)
    event.save()
    response = self.client.patch('/events/%d/' % event.pk, {'end': '2019-01-11 11:00Z'})
    self.assertEqual(response.status_code, 400)

  def test_events_for_day(self):
    events = (Event(name='Within this day', start=p('2019-01-12 12:45Z'), end=p('2019-01-12 13:00Z'), author=self.user),
              Event(name='Starts day before', start=p('2019-01-11 23:45Z'), end=p('2019-01-12 01:00Z'), author=self.user),
              Event(name='Ends next day', start=p('2019-01-12 23:45Z'), end=p('2019-01-13 01:00Z'), author=self.user),
              Event(name='Happens other day', start=p('2019-01-15 12:00Z'), end=p('2019-01-15 13:00Z'), author=self.user))

    for event in events:
      event.save()

    response = self.client.get('/events/day/2019-01-12/')
    names = [event['name'] for event in response.data]
    self.assertNotIn('Happens other day', names)
    in_day = ['Within this day', 'Starts day before', 'Ends next day']
    self.assertTrue(all(name in names for name in in_day))

  def test_events_with_invalid_day(self):
    response = self.client.get('/events/day/2019-13-12/')
    self.assertEqual(response.status_code, 400)

  def test_events_for_month(self):
    events = (Event(name='Within this month', start=p('2019-01-12 12:45Z'), end=p('2019-01-13 13:00Z'), author=self.user),
              Event(name='Starts in previous month', start=p('2018-12-31 23:45Z'), end=p('2019-01-01 01:00Z'), author=self.user),
              Event(name='Ends next month', start=p('2019-01-31 23:45Z'), end=p('2019-02-01 01:00Z'), author=self.user),
              Event(name='Happens other month', start=p('2019-02-15 12:00Z'), end=p('2019-02-15 13:00Z'), author=self.user))

    for event in events:
      event.save()

    response = self.client.get('/events/month/2019-01/')
    names = [event['name'] for event in response.data]
    self.assertNotIn('Happens other month', names)
    in_month = ['Within this month', 'Starts in previous month', 'Ends next month']
    self.assertTrue(all(name in names for name in in_month))
