from rest_framework.permissions import BasePermission

class IsAuthor(BasePermission):
  """ Key permission for event handling. """
  def has_object_permission(self, request, view, obj):
    """ Check permission in a view. """
    return obj.author == request.user