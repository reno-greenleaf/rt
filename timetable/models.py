# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import timedelta
from django.db import models
from django.conf import settings
from celery.task.control import revoke
from tasks import send_reminder

class Event(models.Model):
  """ Event in calendar. """
  OPTIONS = ((timedelta(seconds=3600), '1 hour'),
            (timedelta(seconds=7200), '2 hours'),
            (timedelta(seconds=14400), '4 hours'),
            (timedelta(days=1), '1 day'),
            (timedelta(days=7), '1 week'))

  name = models.CharField(max_length=512)
  start = models.DateTimeField()
  end = models.DateTimeField(blank=True)
  remind = models.DurationField(choices=OPTIONS, default=timedelta(seconds=3600))
  reminder = models.CharField(max_length=100)
  author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='events', on_delete=models.CASCADE)

  def save(self, *args, **kwargs):
    """ Process event before save. """
    # handle end
    if not self.end:
      self.end = self.start.replace(hour=23, minute=59, second=59)

    # handle reminder
    if self.reminder:
      revoke(self.reminder, terminate=True)

    self.reminder = self.create_reminder().id
    super(Event, self).save(*args, **kwargs)

  def create_reminder(self):
    """ Helper for building reminder task. """
    data = ({'start': self.start.ctime(),
             'end': self.end.ctime(),
             'username': self.author.username,
             'name': self.name,
             'email': self.author.email},)
    remind_time = self.start - self.remind
    task = send_reminder.apply_async(data, eta=remind_time)
    return task
