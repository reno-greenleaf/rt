from __future__ import absolute_import, unicode_literals

from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string
from celery import shared_task

@shared_task
def send_reminder(data):
  """ Notify a user about upcoming event. """
  message = render_to_string('timetable/reminder.txt', data)
  send_mail("Upcoming event", message, settings.DEFAULT_FROM_EMAIL, [data['email']])
