# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db.models import Q
from django.core.exceptions import ValidationError
from rest_framework.exceptions import ParseError
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from models import Event
from serializers import EventSerializer
from permissions import IsAuthor

class EventViewSet(ModelViewSet):
  """ Default ways to handle an event. """
  queryset = Event.objects.all()
  serializer_class = EventSerializer
  permission_classes = (IsAuthenticated, IsAuthor)
  authentication_classes = (TokenAuthentication,)

  def get_queryset(self):
    """ List view should show only current users events. """
    return Event.objects.filter(author=self.request.user).all()

  def perform_create(self, serializer):
    """ Current user is author of an event. """
    serializer.save(author=self.request.user)

  @action(detail=False, url_path='day/(?P<date>\d{4}-\d{2}-\d{2})')
  def day(self, request, date):
    """ Events for a day. """
    try:
      events = self.get_queryset().filter(Q(start__date=date) | Q(end__date=date))
    except ValidationError:
      raise ParseError(detail="Provided date is invalid.")

    serializer = self.get_serializer(events, many=True)
    return Response(serializer.data)

  @action(detail=False, url_path='month/(?P<date>\d{4}-\d{2})')
  def month(self, request, date):
    """ Events for a month. """
    # specification says month events should be aggregated somehow;
    # not sure how exactly results should look like for it;
    # so, just showing plain list of events for now.
    year, month_ = date.split('-')
    start = Q(start__year=year) & Q(start__month=month_)
    end = Q(end__year=year) & Q(end__month=month_)
    events = self.get_queryset().filter(start | end)
    serializer = self.get_serializer(events, many=True)
    return Response(serializer.data)
