from models import Event
from rest_framework.serializers import ModelSerializer, ValidationError

class EventSerializer(ModelSerializer):
  """ Serializes an event. """
  class Meta:
    model = Event
    fields = ('id', 'name', 'start', 'end', 'remind')

  def validate(self, data):
    """ Validate full event structure. """
    if 'start' in data:
      start = data['start']
    elif self.instance:
      start = self.instance.start

    if 'end' in data:
      end = data['end']
    elif self.instance:
      end = self.instance.end
    else:
      end = False

    if end and start > end:
      raise ValidationError("Event end is earlier than start.")

    return data
