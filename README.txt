Using Python v2.7 and Django v1.11.20.

Install dependencies:
> pip install celery djangorestframework ics requests
> apt-get install rabbitmq-server

Prepare DB:
> ./manage.py makemigrations
> ./manage.py migrate
> ./manage.py loaddata countries.json
> ./manage.py update_holidays # can take some time; same task is scheduled to run monthly

Run following commands from projects root (in separate terminals). Used for development/debugging environment:
> ./manage.py runserver # web server
> celery -A rt worker -l info -B # celery worker and beat
> python -m smtpd -n -c DebuggingServer localhost:1025 # mail server

If all is set up correctly, following API endpoints should be available:
/events/ - GET list of events and POST an event
/events/<id>/ - GET, PUT, PATCH and DELETE an event
/events/holidays/<month> - list holidays; example of month is 2019-02
/events/day/<date> - list events for a day; example of date is 2019-02-11
/events/month/<date> - list events for a month; example of date is 2019-02
/user/register - POST user data to create inactive user
/user/login/<id>/<token> - GET authentication token
/user/countries - GET list of countries

/user/activate/<id>/<token> - activate user; not an API endpoint, but a page