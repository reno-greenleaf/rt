# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.mail import send_mail
from django.conf import settings
from django.views import View
from django.http import HttpResponse
from django.urls import reverse
from django.shortcuts import get_object_or_404, render
from django.template.loader import render_to_string
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework import status
from serializers import UserSerializer, TokenSerializer, CountrySerializer
from models import User, Country

class Register(APIView):
  """ Create new user. """
  def post(self, request):
    serializer = UserSerializer(data=request.data)

    if serializer.is_valid():
      serializer.save()
      user = serializer.instance
      url = request.build_absolute_uri(reverse('user:activate', args=(user.pk, user.activation_token)))
      message = render_to_string('calendar_user/activation-email.txt', {'user': user, 'url': url})
      send_mail("Account activation", message, settings.DEFAULT_FROM_EMAIL, [user.email])
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    else:
      return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class Activate(View):
  """ Activate registered user. """
  def get(self, request, user_id, token):
    user = get_object_or_404(User, pk=user_id, activation_token=token)
    response = render(request, 'calendar_user/activation.html', {'user': user})

    if not user.is_active:
      user.is_active = True
      user.save()
      url = request.build_absolute_uri(reverse('user:authenticate', args=(user.pk, user.login_token)))
      message = render_to_string('calendar_user/authentication-email.txt', {'user': user, 'url': url})
      send_mail("Login URL", message, settings.DEFAULT_FROM_EMAIL, [user.email])

    return response


class Authenticate(APIView):
  """ Provide access. """
  def get(self, request, user_id, token):
    user = get_object_or_404(User, pk=user_id, login_token=token, is_active=True)
    auth_token = Token.objects.get(user=user)
    serializer = TokenSerializer(auth_token)
    return Response(serializer.data)


class Countries(APIView):
  """ List of countries for clients registration form. """
  def get(self, request):
    countries = Country.objects.all()
    serializer = CountrySerializer(countries, many=True)
    return Response(serializer.data)
