from django.conf.urls import url
from views import Register, Activate, Authenticate, Countries

urlpatterns = [
  url(r'register$', Register.as_view(), name='register'),
  url(r'activate/(?P<user_id>\d+)/(?P<token>\w+)$', Activate.as_view(), name='activate'),
  url(r'login/(?P<user_id>\d+)/(?P<token>\w+)$', Authenticate.as_view(), name='authenticate'),
  url(r'countries$', Countries.as_view(), name='countries')
]
