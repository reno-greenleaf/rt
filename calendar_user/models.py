# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractUser

class Country(models.Model):
  """ Used for a list/select of countries. """
  name = models.CharField(max_length=100, unique=True)


class User(AbstractUser):
  """ Custom user model. """
  activation_token = models.CharField(max_length=100)
  login_token = models.CharField(max_length=100)
  country = models.ForeignKey(Country)
