from django.contrib.auth.password_validation import validate_password
from rest_framework.serializers import ModelSerializer, CharField, EmailField
from rest_framework.authtoken.models import Token
from models import User, Country

class UserSerializer(ModelSerializer):
  """ Serializes custom user. """
  password = CharField(write_only=True)
  email = EmailField(required=True)

  class Meta:
    model = User
    fields = ('id', 'username', 'email', 'password', 'first_name', 'last_name')

  def create(self, validated_data):
    """ Users are created differently than other objects. """
    validated_data['is_active'] = False
    validated_data['activation_token'] = Token().generate_key()
    validated_data['login_token'] = Token().generate_key()
    user = User.objects.create_user(**validated_data)
    Token.objects.create(user=user) # authentication token
    return user

  def validate_password(self, value):
    """ Password needs additional validation. """
    validate_password(value)
    return value


class TokenSerializer(ModelSerializer):
  """ Native AuthTokenSerializer doesn't work for some reason. So, creating custom one. """
  class Meta:
    model = Token
    fields = ('key',)


class CountrySerializer(ModelSerializer):
  """ Serializes country data. """
  class Meta:
    model = Country
    fields = ('id', 'name')
